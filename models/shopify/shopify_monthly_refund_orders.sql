{{ config(materialized='table', alias='monthly_refund_orders') }}

select to_char(PROCESSED_AT::date,'YYYY-MM') as Month_Year,NOTE as Refund_Reason,count(distinct order_id) Order_Count
FROM "FIVETRAN_DATABASE"."SHOPIFY_DS"."REFUND"
GROUP BY 1,2 ORDER BY 1 DESC
{{ config(materialized='table', alias='shopify_discount_price_orders') }}

select a.code,created_at::date as order_date,count(distinct o.order_number) Order_Count,sum(total_discounts) total_discounts
from  "FIVETRAN_DATABASE"."SHOPIFY_DS"."DISCOUNT_APPLICATION" a
join "FIVETRAN_DATABASE"."SHOPIFY_DS"."ORDER" o on a.order_id = o.id
 where code is not null
 group by 1,2
 order by order_count desc